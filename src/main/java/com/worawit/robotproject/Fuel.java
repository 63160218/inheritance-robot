/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

/**
 *
 * @author Melon
 */
public class Fuel extends Obj {

    int volumn;

    public Fuel(int x, int y, int volumn) {
        super('F', x, y);
        this.volumn = volumn;
    }

    public int fillFuel() {
        int vol = volumn;
        symbol = '-';
        volumn = 0;
        return vol;
    }

    public int getVolumn() {
        return volumn;
    }

}
