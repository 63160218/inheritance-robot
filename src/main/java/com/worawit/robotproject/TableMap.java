/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

/**
 *
 * @author werapan
 */
public class TableMap {

    private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;
    private Obj[] objects = new Obj[100];
    int objCount = 0;
    

    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void addObj(Obj obj) {
        objects[objCount] = obj;
        objCount++;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
        addObj(robot);
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
        addObj(bomb);
    }

    public void printSymbolOn(int x, int y) {
        char symbol = '-';
        for (int o = 0; o < objCount; o++) {
            if (objects[o].isOn(x, y)) {
                symbol = objects[o].getSymbol();
            }
        }
        System.out.print(symbol);
    }

    public void showMap() {
        showTitle();
        System.out.println("Fuel: "+ robot.getF());
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                printSymbolOn(x, y);
            }
            showNewLine();
        }

    }

    private void showTitle() {
        System.out.println("Map");
    }

    private void showNewLine() {
        System.out.println("");
    }

    private void showCell() {
        System.out.print("-");
    }

    private void showBomb() {
        System.out.print(bomb.getSymbol());
    }

    private void showObj(Obj obj) {
        System.out.print(obj.getSymbol());
    }

    private void showRobot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {
        // x -> 0-(width-1), y -> 0-(height-1)
        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }

    public boolean isBomb(int x, int y) {
        return bomb.isOn(x, y);
    }
    
    public boolean isTree(int x, int y) {
        for (int o = 0; o < objCount; o++) {
            if (objects[o] instanceof Tree && objects[o].isOn(x, y)) {
               return true;
            }
        }
        return false;
    }
    
    public int  fillFuel(int x, int y) {
        for (int o = 0; o < objCount; o++) {
            if (objects[o] instanceof Fuel && objects[o].isOn(x, y)) {
              Fuel fuel = (Fuel) objects[o];
              return fuel.fillFuel();
            }
        }
        return 0;
    }
}
